package ru.traker.exceptions;

public class UserAlreadyExistsException extends UserException{
    public UserAlreadyExistsException(String message) {
        super(message, "Дубликат почты");
    }
}
