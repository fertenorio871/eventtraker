package ru.traker.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;
import org.springframework.web.bind.annotation.RequestParam;
import ru.traker.dto.UserDto;
import ru.traker.dto.UserRegisterRequestDto;
import ru.traker.exceptions.UserAlreadyExistsException;
import ru.traker.models.Role;
import ru.traker.models.User;
import ru.traker.repos.UserRepo;
import java.util.Set;

@Service
public class UserService {

    UserRepo userRepo;
    @Autowired
    Validator validator;


    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public void saveUser(UserRegisterRequestDto userRegisterRequestDto){

        userWithThisEmailAlreadyExists(userRegisterRequestDto);

        userRepo.save(
                User.builder().authorities(Set.of(Role.ROLE_USER))
                .active(false)
                .email(userRegisterRequestDto.getEmail())
                .password(userRegisterRequestDto.getPassword())
                .username(userRegisterRequestDto.getUsername()).build());
    }


    public void userWithThisEmailAlreadyExists(UserRegisterRequestDto user){
       if(userRepo.findByEmail(user.getEmail()).isPresent()){
           throw new UserAlreadyExistsException("пользователь с такой почтой уже существует");
       }
    }

    public ResponseEntity<?> getAllUsers(){
        return ResponseEntity.ok(userRepo.findAll());
    }

    public ResponseEntity<?> deleteUser(Long id){
        userRepo.deleteById(id);
        return ResponseEntity.ok("Пользователь удалён");
    }

}
